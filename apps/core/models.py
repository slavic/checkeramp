import uuid
from django.db import models
from django.utils.text import slugify


class Sample(models.Model):
    TRESHOLD = 0.5

    sequence = models.TextField(
        'Sequence',
        blank=True
    )
    fasta_file = models.FileField(
        'Fasta',
        upload_to='fastas',
        blank=True
    )

    result = models.TextField(
        'Result',
        blank=True
    )

    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        db_index=True
    )

    slug = models.SlugField(
        unique=True,
        editable=False,
        max_length=150
    )

    created_date = models.DateTimeField(
        'Data created',
        auto_now_add=True
    )

    def _set_slug(self):
        self.slug = slugify(self.uuid, allow_unicode=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            super(Sample, self).save(*args, **kwargs)

        self._set_slug()

        super(Sample, self).save()

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'sample'
        verbose_name_plural = 'samples'

        ordering = ['-created_date']
