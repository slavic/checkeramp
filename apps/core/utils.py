import os
from io import StringIO
from typing import List

import pandas as pd

from Bio.SeqIO.FastaIO import FastaIterator
from Bio.SeqRecord import SeqRecord


class FastaReader:
    def __init__(self, fasta_file_path: str):
        self.fasta_file_path = fasta_file_path
        self.fasta_name = os.path.basename(self.fasta_file_path)
        self.sequence = None

    @staticmethod
    def _fasta_reader(filename: str) -> SeqRecord:
        with open(filename) as handle:
            for record in FastaIterator(handle):
                yield record

    @staticmethod
    def _normalize(entry: SeqRecord):
        return str(entry.seq).upper().strip()

    def get_sequences(self) -> List[tuple]:
        sequences = []

        for entry in self._fasta_reader(self.fasta_file_path):
            sequences.append((entry.id, self._normalize(entry)))

        return sequences


class CSVGenerator:
    def __init__(self, result):
        self.result = result

    def to_csv(self):
        buffer = StringIO()

        dt = pd.DataFrame(
            self.result
        ).rename(
            columns={
                'id': 'ID',
                'data': 'Sequence',
                'proba': 'Probability'
            }
        )

        dt['Predicted class'] = dt['Probability'].map(lambda proba: 'AMP' if proba > 50 else 'Non-AMP')

        dt.to_csv(
            buffer,
            index=False,
            columns=['ID', 'Sequence', 'Predicted class', 'Probability']
        )

        buffer.seek(0)

        return buffer.read()
