from django.urls import path, re_path

from .views import DashbordView, SampleDisplayView, SampleStatusView, SampleCsvView

urlpatterns = [
    path(
        r'',
        DashbordView.as_view(),
        name='dashboard'
    ),
    re_path(
        r'samples/(?P<slug>[\w-]+)/status/$',
        SampleStatusView.as_view(),
        name='sample_status_view'
    ),
    re_path(
        r'samples/(?P<slug>[\w-]+)/csv/$',
        SampleCsvView.as_view(),
        name='sample_csv_view'
    ),
    re_path(
        r'samples/(?P<slug>[\w-]+)/$',
        SampleDisplayView.as_view(),
        name='sample_display_view'
    ),
]
