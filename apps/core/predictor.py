from typing import List

import pandas as pd
from tensorflow.keras import preprocessing
from tensorflow.keras.models import load_model


class Predictor:
    amino_acid_to_index = {
        'A': 1,
        'C': 2,
        'D': 3,
        'E': 4,
        'F': 5,
        'G': 6,
        'H': 7,
        'I': 8,
        'K': 9,
        'L': 10,
        'M': 11,
        'N': 12,
        'P': 13,
        'Q': 14,
        'R': 15,
        'S': 16,
        'T': 17,
        'V': 18,
        'W': 19,
        'Y': 20,
    }

    MAX_LENGTH = 200

    ADDITIONAL_MODELS = {
        'AMP': 'models/01_21.h5',
        'Antiviral': 'models/antiviral.h5',
        'Gram-negative': 'models/bacteria-neg.h5',
        'Gram-positive': 'models/bacteria-pos.h5',
    }

    def __init__(self):
        self.additional_models = {
            model_name: load_model(model_path) for model_name, model_path in self.ADDITIONAL_MODELS.items()
        }

    def _prepare_features(self, sequences: List[str]):
        features = [
            [self.amino_acid_to_index[amino_acid] if amino_acid in self.amino_acid_to_index else 0 for amino_acid in
             sequence]
            for sequence in sequences
        ]

        return preprocessing.sequence.pad_sequences(features, maxlen=self.MAX_LENGTH)

    def _predict(self, features, model):
        predicted_probabilities = model.predict(features, batch_size=512)
        return [
            round(float(prediction) * 100, 2) for prediction in predicted_probabilities
        ]

    def predict(self, sequences):
        features = self._prepare_features(sequences)

        predictions = {}

        for model_name, model in self.additional_models.items():
            predictions[model_name] = self._predict(features, model)

        return list(pd.DataFrame(predictions).transpose().to_dict().values())
