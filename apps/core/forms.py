from io import StringIO

from django import forms
from django.forms import ModelForm
from django.core.files.base import ContentFile

from Bio import SeqIO

from .models import Sample


class SampleForm(ModelForm):
    sequence = forms.CharField(
        widget=forms.Textarea(attrs={"rows": 10, "cols": 100, "class": "form-control"}),
        required=False
    )

    def clean(self):
        cleaned_data = super(SampleForm, self).clean()

        sequence = cleaned_data.get('sequence')
        fasta_file = cleaned_data.get('fasta_file')

        if not sequence and not fasta_file:
            raise forms.ValidationError({
                'sequence': 'This field is required when FASTA is empty',
                'fasta_file': 'This field is required when sequence is empty'
            })

        if sequence and not list(SeqIO.parse(StringIO(sequence), "fasta")):
            raise forms.ValidationError({
                'sequence': 'Invalid sequence format',
            })

        return cleaned_data

    def save(self, commit=True):
        sample = super(SampleForm, self).save(commit=False)

        if commit:
            if sample.sequence:
                sample.fasta_file.save(
                    f'{sample.uuid}.fasta',
                    ContentFile(sample.sequence)
                )

            sample.save()

        return sample

    class Meta:
        model = Sample

        fields = '__all__'
