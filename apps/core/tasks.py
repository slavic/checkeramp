import json
import operator
from celery.utils.log import get_task_logger

from typing import Optional

from .models import Sample

from checkeramp.celery import app

from .utils import FastaReader
from .predictor import Predictor

logger = get_task_logger(__name__)

predictor: Optional[Predictor] = None


@app.task(bind=True)
def amp_predictor(self, sample_id: int):
    sample = Sample.objects.get(pk=sample_id)

    fr = FastaReader(sample.fasta_file.path)
    ids_with_sequences = fr.get_sequences()

    global predictor
    if not predictor:
        predictor = Predictor()

        logger.debug('[DEBUG] Predictor object created')

    sequence_data_for_predictor = list(map(operator.itemgetter(1), ids_with_sequences))

    predictions = predictor.predict(sequence_data_for_predictor)

    def create_sample_info(sequence_id, sequence, predictions_for_sequence):
        amp_prediction = predictions_for_sequence.pop('AMP')
        return {
            'id': sequence_id,
            'data': sequence,
            'proba': amp_prediction,
            'additional_probabilities': predictions_for_sequence
        }

    result = [
        create_sample_info(data[0], data[1], predictions_for_sample) for data, predictions_for_sample in zip(
            ids_with_sequences,
            predictions
        )
    ]

    sample.result = json.dumps(result)

    sample.save()
