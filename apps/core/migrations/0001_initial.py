# Generated by Django 2.2.6 on 2019-10-05 13:31

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sample',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sequence', models.TextField(blank=True, verbose_name='Sequence')),
                ('fasta_file', models.FileField(blank=True, upload_to='fastas', verbose_name='Fasta')),
                ('probability', models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True, verbose_name='Probability')),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False)),
                ('slug', models.SlugField(editable=False, max_length=150, unique=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Data created')),
            ],
            options={
                'verbose_name': 'sample',
                'verbose_name_plural': 'samples',
                'ordering': ['-created_date'],
            },
        ),
    ]
