import json

from django.http import HttpResponse
from django.views.generic import View
from django.shortcuts import render, redirect
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

from .forms import SampleForm
from .tasks import amp_predictor
from .models import Sample
from .utils import CSVGenerator


class DashbordView(View):
    template_name = 'index.html'

    def get(self, request):
        sample_form = SampleForm()

        context = {
            'sample_form': sample_form
        }

        return render(
            request,
            template_name=self.template_name,
            context=context
        )

    def post(self, request):
        sample_form = SampleForm(
            request.POST or None,
            request.FILES or None
        )

        if sample_form.is_valid():
            sample = sample_form.save()

            amp_predictor.delay(sample.pk)

            return redirect('sample_display_view', slug=sample.slug)
        else:
            messages.add_message(
                request,
                messages.ERROR,
                f'Processing failed. Check the details below.'
            )

        context = {
            'sample_form': sample_form
        }

        return render(
            request,
            template_name=self.template_name,
            context=context
        )


class SampleDisplayView(View):
    template_name = 'sample.html'

    def get(self, request, slug):
        sample = Sample.objects.get(slug=slug)

        if sample.result:
            result = json.loads(sample.result)
        else:
            result = []

        context = {
            'sample': sample,
            'result': result,
            'result_len': len(result)
        }

        return render(
            request,
            template_name=self.template_name,
            context=context
        )


class SampleCsvView(View):
    def get(self, request, slug):
        sample = Sample.objects.get(slug=slug)

        if sample.result:
            result = json.loads(sample.result)
        else:
            result = []

        csv = CSVGenerator(result=result)
        content = csv.to_csv()

        response = HttpResponse(content=content, content_type='application/csv')
        response['Content-Disposition'] = f'attachment; filename="{sample.slug}.csv"'

        return response


class SampleStatusView(View):
    def get(self, request, slug):
        sample = Sample.objects.get(slug=slug)

        if sample.result:
            return HttpResponse(content='DONE')
        else:
            return HttpResponse(content='IN_PROGRESS')
