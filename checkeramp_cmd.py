#!/usr/bin/env python3

import os
import requests
import argparse
import time


class CheckerAMPCommand:
    upload_url = 'http://localhost:8000/'

    TIMEOUT = 300

    def __init__(self, fasta_path):
        self.fasta_path = fasta_path

        self.client = None

        self.csrftoken = self._get_csrf()

    def _get_csrf(self):
        self.client = requests.session()
        self.client.get(self.upload_url)
        csrftoken = self.client.cookies['csrftoken']

        return csrftoken

    def upload_file(self):
        result = self.client.post(
            self.upload_url,
            timeout=self.TIMEOUT,
            data={
                'sequence': open(self.fasta_path).read(),
                'csrfmiddlewaretoken': self.csrftoken
            },
        )

        if result.status_code == 200:
            print('[DEBUG] File uploaded successfully')

            csv_is_ready = 'IN_PROGRESS'
            while csv_is_ready != 'DONE':
                csv_is_ready = self.client.get(
                    f'{result.url}status/',
                    timeout=self.TIMEOUT,
                ).text

                time.sleep(1)

            csv_source = self.client.get(
                f'{result.url}csv/',
                timeout=self.TIMEOUT,
            )

            csv_filename = os.path.splitext(os.path.basename(self.fasta_path))[0]
            open(f'{csv_filename}.csv', 'wb').write(csv_source.content)

            print(f'[DEBUG] {csv_filename}.csv saved')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CheckerAMP command line tool')
    parser.add_argument(
        '--fasta_path',
        action='store',
        type=str,
        required=True,
        help='FASTA file path'
    )

    args = parser.parse_args()

    champ = CheckerAMPCommand(fasta_path=args.fasta_path)
    champ.upload_file()
