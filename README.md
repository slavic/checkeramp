# CheckerAMP

CheckerAMP is a command line tool and web platform for identification of antimicrobial peptides (AMPs).

Use CheckerAMP as a command line tool:
```bash
./checkeramp_cmd.py --fasta_path=multifasta_example.fasta
```

Next, explore script results by `multifasta_example.csv` CSV file with comma separated data:

```csv
ID,Sequence,Predicted class,Probability
amp5_30_1499,GLKDWVKIAGGWLKKKGPGILKAAMAAATQ,AMP,98.46
amp5_30_1500,GLLLDTVKGAAKNVAGILLNKLKCKMTGDC,AMP,97.57
amp5_30_1501,GLPTCGETCFGGTCNTPGCTCDPWPVCTHN,AMP,98.07
amp5_30_1502,GLPVCGETCFGGTCNTPGCTCDPWPVCTRN,AMP,98.24
amp5_30_1503,GLPVCGETCFTGTCYTNGCTCDPWPVCTRN,AMP,98.32
amp5_30_1504,GNAACVIGCIGSCVISEGIGSLVGTAFTLG,AMP,97.05
```

Use CheckerAMP as a web platform:

```bash
./manage.py runserver & ./run_celery.sh
```

Next, use your browser and go to http://localhost:8000/

# Version
Numer: 1.0.0  
Date released: 06.10.2019

# Contact
SLAVIC team:
* Michał Jadczuk
* Marcin Lubocki
* Piotr Tynecki
* Łukasz Wałejko
